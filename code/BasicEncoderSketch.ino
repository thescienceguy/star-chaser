#define ENCODER_CLK_PIN 2
#define ENCODER_DATA_PIN 4
#define PULSES_PER_ROTATION 600

// This must be declared as volatile as it changes in the interrupt function
volatile uint16_t counter = 0;

// This allows for the adjustment of arbitary wiring to ensure the encoder is counting in the correct direction
int flip_direction = 1;

void clockRise()
{

   counter += flip_direction * (digitalRead(ENCODER_DATA_PIN) == HIGH ? 1 : -1);
}

void setup()
{
   pinMode(ENCODER_CLK_PIN, INPUT_PULLUP);
   pinMode(ENCODER_DATA_PIN, INPUT_PULLUP);
   attachInterrupt(digitalPinToInterrupt(ENCODER_CLK_PIN), clockRise, RISING);
   Serial.begin(9600);
}

void loop()
{
   Serial.println(counter % PULSES_PER_ROTATION);
}