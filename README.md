# Star Chaser: Automatic Dome Controller

The Star Chaser dome control project was led by Kris Turner as part an upgrade, commissioned by Keele Observatory, in order to improve the functionality of their 24" research, reflector telescope.

Keele Observatory is located in North Staffordshire,  between Manchester and Birmingham on a large estate with extensive woods, lakes and parkland, formerly owned by the Sneyd family. 97% of the university's research is classified as world leading and of international importance, as a result of the 2014 Research Excellence Framework (REF 2014).

The software and digital electronics were produced by **Kris Turner**, who at the time of the project, was an undergraduate physics with chemistry student. The mechanical designs were produced by **Dr James Albinson**. **Alan Bagnell** advised on the design, and safety, of the electrical systems.

*A special mention must be made to the entirety of the Keele Observatory volunteers without who there would be no dome to upgrade.*

[View Documentation](https://gitlab.com/thescienceguy/star-chaser/wikis/home)